<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsumableTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consumable_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('consumable_id')->unsigned();
            $table->string('locale')->index();

            $table->string('name', 100);
            $table->string('slug', 150)->unique();
            $table->text('description')->nullable();

            $table->unique(['consumable_id', 'locale']);
            $table->foreign('consumable_id')->references('id')->on('consumables')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consumable_translations');
    }
}
