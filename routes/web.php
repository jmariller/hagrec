<?php

// Main menu
Route::get('/', 'HomeController@index')->name('home');
Route::get(trans('routes.products'), 'ProductsController@index')->name('products');
Route::get(trans('routes.products').'/{slug}', 'ProductsController@show')->name('product.show');
//Route::get(trans('routes.consumables'), 'ConsumablesController@index')->name('consumables');
//Route::get(trans('routes.consumables').'/{slug}', 'ConsumablesController@show')->name('consumable.show');
//Route::get(trans('routes.references'), 'ReferencesController@index')->name('references');
Route::get(trans('routes.contact'), 'ContactController@index')->name('contact');

// Language selection
Route::get('lang/{language}', 'LanguageController@switchLang')->name('lang.switch');

// Login page and admin section
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// General administration
Route::get('admin', 'AdminController@index')->name('admin');
Route::get('admin/hagrec', 'AdminHagrecController@index')->name('admin.hagrec');
Route::get('admin/hagrec/suppliers', 'AdminSuppliersController@index')->name('admin.hagrec.suppliers');
Route::get('admin/hagrec/suppliers/1/edit', 'AdminSuppliersController@edit')->name('admin.hagrec.suppliers.edit');
Route::get('admin/miltek', 'AdminMiltekController@index')->name('admin.miltek');

// Products administration
Route::get('products/create', 'ProductsController@create')->name('product.create');
Route::post('products', 'ProductsController@store')->name('product.store');
Route::get('products/{product}/edit', 'ProductsController@edit')->name('product.edit');
Route::put('products/{product}', 'ProductsController@update')->name('product.update');
Route::put('products', 'ProductsController@updateAll')->name('product.update_all');
Route::delete('products/{product}', 'ProductsController@destroy')->name('product.destroy');

// Consumables administration
//Route::get('consumables/create', 'ConsumablesController@create')->name('consumable.create');
//Route::post('consumables', 'ConsumablesController@store')->name('consumable.store');
//Route::get('consumables/{consumable}/edit', 'ConsumablesController@edit')->name('consumable.edit');
//Route::put('consumables/{consumable}', 'ConsumablesController@update')->name('consumable.update');
//Route::put('consumables', 'ConsumablesController@updateAll')->name('consumable.update_all');
//Route::delete('consumables/{consumable}', 'ConsumablesController@destroy')->name('consumable.destroy');

// References administration
//Route::post('references', 'ReferencesController@store')->name('reference.store');
//Route::put('references/{reference}', 'ReferencesController@update')->name('reference.update');
//Route::put('references', 'ReferencesController@updateAll')->name('reference.update_all');
//Route::delete('references/{reference}', 'ReferencesController@destroy')->name('reference.destroy');
//Route::post('references/{reference}/logo', 'ReferenceLogoController@store')->name('reference_logo.store');
