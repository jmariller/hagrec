<?php

use App\Consumable;
use App\Product;

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push(trans('hagrec.home'), route('home'));
});

// Home > Products
Breadcrumbs::for('products', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('hagrec.products'), route('products'));
});

// Home > Products > [Product]
Breadcrumbs::for('product.show', function ($trail, $slug) {
    try {
        $product = Product::whereTranslation('slug', $slug)->firstOrFail();

        $trail->parent('products');
        $trail->push($product->name, route('product.show', $product->slug));
    } catch (Exception $e) {
        $trail->parent('products');
    }
});

// Home > Consumables
Breadcrumbs::for('consumables', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('hagrec.consumables'), route('consumables'));
});

// Home > Consumables > [Consumable]
Breadcrumbs::for('consumable.show', function ($trail, $slug) {
    try {
        $consumable = Consumable::whereTranslation('slug', $slug)->firstOrFail();

        $trail->parent('consumables');
        $trail->push($consumable->name, route('consumable.show', $consumable->slug));
    } catch (Exception $e) {
        $trail->parent('consumables');
    }
});

// Home > References
Breadcrumbs::for('references', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('hagrec.references'), route('references'));
});

// Home > Contact
Breadcrumbs::for('contact', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('hagrec.contact'), route('contact'));
});

// Login
Breadcrumbs::for('login', function($trail) {
    $trail->parent('home');
    $trail->push('Login', route('login'));
});

// Admin
Breadcrumbs::for('admin', function($trail) {
    $trail->parent('home');
    $trail->push('Administration', route('admin'));
});

// Admin > Hagrec
Breadcrumbs::for('admin.hagrec', function($trail) {
    $trail->parent('admin');
    $trail->push('Hagrec SA', route('admin.hagrec'));
});

// Admin > Hagrec > Suppliers
Breadcrumbs::for('admin.hagrec.suppliers', function($trail) {
    $trail->parent('admin.hagrec');
    $trail->push('Fournisseurs', route('admin.hagrec.suppliers'));
});

// Admin > Hagrec > Suppliers > Edit
Breadcrumbs::for('admin.hagrec.suppliers.edit', function($trail) {
    $trail->parent('admin.hagrec.suppliers');
    $trail->push('Fournisseurs - Modification', route('admin.hagrec.suppliers.edit'));
});

// Admin > Mil-Tek
Breadcrumbs::for('admin.miltek', function($trail) {
    $trail->parent('admin');
    $trail->push('Mil-tek A/S', route('admin.miltek'));
});

// Admin > Products > Create
Breadcrumbs::for('product.create', function($trail) {
    $trail->parent('products');
    $trail->push('Créer un nouveau produit', route('product.create'));
});

// Admin > Products > Edit
Breadcrumbs::for('product.edit', function($trail, $product) {
    $trail->parent('products');
    $trail->push($product->name, route('product.show', $product->slug));
    $trail->push('Modifier', route('product.edit', $product));
});

// Admin > Consumables > Create
Breadcrumbs::for('consumable.create', function($trail) {
    $trail->parent('consumables');
    $trail->push('Créer un nouveau consommable', route('consumable.create'));
});

// Admin > Consumables > Edit
Breadcrumbs::for('consumable.edit', function($trail, $consumable) {
    $trail->parent('consumables');
    $trail->push($consumable->name, route('consumable.show', $consumable->slug));
    $trail->push('Modifier', route('consumable.edit', $consumable));
});

// 404 - Not found
Breadcrumbs::for('errors.404', function ($trail) {
    $trail->parent('home');
});
Breadcrumbs::for('lang.switch', function ($trail) {});
