<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class ConsumableTranslation extends Model
{
    use Sluggable;

    public $timestamps = false;
    protected $fillable = ['name', 'slug', 'description'];

    /**
     * Define the consumable slug
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => ['consumable_id', 'name']
            ]
        ];
    }
}
