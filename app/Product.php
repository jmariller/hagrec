<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Dimsav\Translatable\Translatable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use Translatable;

    public $translatedAttributes = ['name', 'slug', 'description', 'tech_specs', 'other_info'];
    protected $fillable = ['highlight'];
    protected $appends = ['image', 'url'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * Get the default small image
     *
     * @return string
     */
    public function getImageAttribute()
    {
        return $this->attributes['image'] = $this->image(1, 'small');
    }

    /**
     * Get the URL to the product
     *
     * @return string
     */
    public function getUrlAttribute()
    {
        return $this->attributes['url'] = route('product.show', $this->slug);
    }

    /**
     * Get the products highlight
     *
     * @return mixed
     */
    public static function highlight()
    {
        return static::where('highlight', true)->first();
    }

    /**
     * Reset any highlight
     *
     * @return int
     */
    public static function resetHighlight()
    {
        return DB::table('products')
                    ->where('highlight', true)
                    ->update(['highlight' => false]);
    }

    /**
     * Retrieve a single image's path
     *
     * @param int    $nb
     * @param string $suffix
     * @return string
     */
    public function image(int $nb = 1, string $suffix = null)
    {
        return $this->media('images', 'jpg', $suffix, 'default.png', $nb);
    }

    /**
     * Retrieve the video's path
     *
     * @return string
     */
    public function video()
    {
        return $this->media('videos', 'mp4');
    }

    /**
     * Retrieve the PDF's path
     *
     * @param int    $nb
     * @param string $locale
     * @return string
     */
    public function pdf(int $nb = 1, $locale = 'fr')
    {
        return $this->media('pdfs', 'pdf', $locale, null, $nb);
    }

    /**
     * Retrieve a media's path (image / video / pdf)
     *
     * @param string $folder
     * @param string $extension
     * @param string $suffix
     * @param string $default
     * @param int    $nb
     * @return string
     */
    private function media(string $folder, string $extension, string $suffix = null, string $default = null, int $nb = null)
    {
        $file = $this->id . ($nb != null ? "-{$nb}" : '') . ($suffix != null ? "-{$suffix}" : '') . ".{$extension}";

        if (! Storage::disk('public')->exists("{$folder}/products/{$this->id}/{$file}")) {
            return $default == null ? '' : asset("storage/{$folder}/products/{$default}");
        }

        $path = asset("storage/{$folder}/products/{$this->id}/{$file}");

        return $path;
    }
}
