<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Consumable extends Model
{
    use Translatable;

    public $translatedAttributes = ['name', 'slug', 'description'];
    protected $fillable = ['name', 'description', 'image_path', 'sort_order'];
    protected $appends = ['image', 'url'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * Get the image
     *
     * @return string
     */
    public function getImageAttribute()
    {
        return $this->attributes['image'] = $this->image(1, 'small');
    }

    /**
     * Get the URL to the consumable
     *
     * @return string
     */
    public function getUrlAttribute()
    {
        return $this->attributes['url'] = route('consumable.show', $this->slug);
    }

    /**
     * Retrieve a single image's path
     *
     * @param int    $nb
     * @param string $suffix
     * @return string
     */
    public function image(int $nb = 1, string $suffix = null)
    {
        return $this->media('images', 'jpg', $suffix, 'default.png', $nb);
    }

    /**
     * Retrieve a media's path (image / video / pdf)
     *
     * @param string $folder
     * @param string $extension
     * @param string $suffix
     * @param string $default
     * @param int    $nb
     * @return string
     */
    private function media(string $folder, string $extension, string $suffix = null, string $default = null, int $nb = null)
    {
        $file = $this->id . ($nb != null ? "-{$nb}" : '') . ($suffix != null ? "-{$suffix}" : '') . ".{$extension}";

        if (! Storage::disk('public')->exists("{$folder}/consumables/{$this->id}/{$file}")) {
            return $default == null ? '' : asset("storage/{$folder}/consumables/{$default}");
        }

        $path = asset("storage/{$folder}/consumables/{$this->id}/{$file}");

        return $path;
    }
}
