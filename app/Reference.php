<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Reference extends Model
{
    protected $fillable = ['name', 'logo_path', 'sort_order'];
    protected $appends = ['image'];

    /**
     * Get the image
     *
     * @return string
     */
    public function getImageAttribute()
    {
        return $this->attributes['image'] = $this->pathToLogo();
    }

    /**
     * Get the file name
     *
     * @return string
     */
    public function fileName()
    {
        return str_slug($this->name) . '-logo.png';
    }

    /**
     * Determine the path to the logo for that reference
     *
     * @return string
     */
    public function pathToLogo()
    {
        if ($this->logo_path == null) return null;

        return asset("storage/images/references/$this->logo_path");
    }

    /**
     * Delete the logo
     */
    public function deleteLogo()
    {
        if ($this->logo_path == null) return;

        Storage::disk('public')->delete("images/references/$this->logo_path");
    }

}
