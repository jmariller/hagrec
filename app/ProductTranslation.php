<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class ProductTranslation extends Model
{
    use Sluggable;

    public $timestamps = false;
    protected $fillable = ['name', 'slug', 'description', 'tech_specs', 'other_info'];

    /**
     * Define the product slug
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => ['product_id', 'name']
            ]
        ];
    }
}
