<?php

namespace App\Http\Requests;

use App\Consumable;
use Illuminate\Foundation\Http\FormRequest;
use Intervention\Image\ImageManagerStatic as Image;

class StoreConsumable extends FormRequest
{
    const SMALL_IMAGE_HEIGHT = 300;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name.fr' => 'required',
            'name.de' => 'required'
        ];
    }

    /**
     * Save consumable to database
     *
     * @param Consumable $consumable
     * @return Consumable
     */
    public function persist(Consumable $consumable)
    {
        $this->highlight($consumable);
        $this->translations($consumable);

        $consumable->save();

        return $consumable;
    }

    /**
     * Set the highlight
     *
     * @param Consumable $consumable
     */
    private function highlight(Consumable $consumable)
    {
        if (request('highlight') == 'on') {
            Consumable::resetHighlight();
            $consumable->highlight = true;
        }
    }

    /**
     * Set the consumable translations
     *
     * @param Consumable $consumable
     * @return Consumable
     */
    private function translations(Consumable $consumable)
    {
        $translatable_inputs = ['name', 'description'];

        foreach ($translatable_inputs as $input) {
            foreach (request($input) as $locale => $value) {
                $consumable->translateOrNew($locale)->$input = $value;
            }
        }

        return $consumable;
    }

    /**
     * Save the attachments of the consumable
     * @param Consumable $consumable
     */
    public function saveAttachments(Consumable $consumable)
    {
        $this->saveImages($consumable);
    }

    /**
     * Save images and their miniatures in "storage/app/public/images/consumables/{$consumable->id}/{$consumable->id}-{$nb}(-small).jpg"
     *
     * @param Consumable $consumable
     * @param int     $nb
     */
    private function saveImages(Consumable $consumable, int $nb = 1)
    {
        $this->storeAttachments($consumable, 'image', "{$consumable->id}-{$nb}", 'jpg', function($consumable, $path, $nb) {
            $this->createSmallImage($consumable, $path, $nb);
        });
    }

    /**
     * Create small picture in "storage/app/public/images/consumables/{$consumable->id}/{$consumable->id}-{$nb}-small.jpg"
     *
     * @param Consumable $consumable
     * @param string  $path
     * @param int     $nb
     */
    private function createSmallImage(Consumable $consumable, string $path, int $nb)
    {
        $root = config('filesystems.disks.public.root');

        $img = Image::make("$root/$path")->heighten(self::SMALL_IMAGE_HEIGHT);

        $img->save("$root/images/consumables/{$consumable->id}/{$consumable->id}-{$nb}-small.jpg");
    }

    /**
     * Store one or multiple attachment(s) (image / video / pdf) in "storage/app/public/{$name}s/consumables/{$consumable->id}"
     *
     * @param Consumable       $consumable
     * @param string        $name
     * @param string        $file_name
     * @param string        $extension
     * @param callable|null $callback
     * @param int           $nb
     */
    private function storeAttachments(Consumable $consumable, string $name, string $file_name, string $extension, callable $callback = null, int $nb = 1)
    {
        if (request()->hasFile($name)) {
            if (is_array(request()->file($name))) {
                foreach (request()->file($name) as $locale => $attachment) {
                    $this->storeAttachment($attachment, $consumable, $name, str_replace_last('{locale}', $locale, $file_name), $extension, $callback, $nb);
                }
            } else {
                $this->storeAttachment(request()->file($name), $consumable, $name, $file_name, $extension, $callback, $nb);
            }
        }
    }

    /**
     * Store a given attachment in "storage/app/public/{$name}s/consumables/{$consumable->id}"
     *
     * @param               $attachment
     * @param Consumable       $consumable
     * @param string        $name
     * @param string        $file_name
     * @param string        $extension
     * @param callable|null $callback
     * @param int           $nb
     */
    private function storeAttachment($attachment, Consumable $consumable, string $name, string $file_name, string $extension, callable $callback = null, int $nb = 1)
    {
        if ($attachment->isValid()) {
            $path = $attachment->storeAs("{$name}s/consumables/{$consumable->id}", "{$file_name}.{$extension}", 'public');

            if (is_callable($callback)) {
                $callback($consumable, $path, $nb);
            }
        }
    }
}
