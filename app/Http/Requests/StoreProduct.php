<?php

namespace App\Http\Requests;

use App\Product;
use Illuminate\Foundation\Http\FormRequest;
use Intervention\Image\ImageManagerStatic as Image;

class StoreProduct extends FormRequest
{
    const SMALL_IMAGE_HEIGHT = 300;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name.fr' => 'required',
            'name.de' => 'required'
        ];
    }

    /**
     * Save product to database
     *
     * @param Product $product
     * @return Product
     */
    public function persist(Product $product)
    {
        $this->highlight($product);
        $this->translations($product);

        $product->save();

        return $product;
    }

    /**
     * Set the highlight
     *
     * @param Product $product
     */
    private function highlight(Product $product)
    {
        if (request('highlight') == 'on') {
            Product::resetHighlight();
            $product->highlight = true;
        }
    }

    /**
     * Set the product translations
     *
     * @param Product $product
     * @return Product
     */
    private function translations(Product $product)
    {
        $translatable_inputs = ['name', 'description', 'tech_specs', 'other_info'];

        foreach ($translatable_inputs as $input) {
            foreach (request($input) as $locale => $value) {
                $product->translateOrNew($locale)->$input = $value;
            }
        }

        return $product;
    }

    /**
     * Save the attachments of the product
     * @param Product $product
     */
    public function saveAttachments(Product $product)
    {
        $this->saveImages($product);
        $this->saveVideo($product);
        $this->savePdfs($product);
    }

    /**
     * Save images and their miniatures in "storage/app/public/images/products/{$product->id}/{$product->id}-{$nb}(-small).jpg"
     *
     * @param Product $product
     * @param int     $nb
     */
    private function saveImages(Product $product, int $nb = 1)
    {
        $this->storeAttachments($product, 'image', "{$product->id}-{$nb}", 'jpg', function($product, $path, $nb) {
            $this->createSmallImage($product, $path, $nb);
        });
    }

    /**
     * Create small picture in "storage/app/public/images/products/{$product->id}/{$product->id}-{$nb}-small.jpg"
     *
     * @param Product $product
     * @param string  $path
     * @param int     $nb
     */
    private function createSmallImage(Product $product, string $path, int $nb)
    {
        $root = config('filesystems.disks.public.root');

        $img = Image::make("$root/$path")->heighten(self::SMALL_IMAGE_HEIGHT);

        $img->save("$root/images/products/{$product->id}/{$product->id}-{$nb}-small.jpg");
    }

    /**
     * Save video in "storage/app/public/videos/products/{$product->id}/{$product->id}.mp4"
     *
     * @param Product $product
     */
    private function saveVideo(Product $product)
    {
        $this->storeAttachments($product, 'video', $product->id, 'mp4');
    }

    /**
     * Save PDFs in "storage/app/public/pdfs/products/{$product->id}/{$product->id}-{$nb}-{locale}.pdf"
     *
     * @param Product $product
     * @param int     $nb
     */
    private function savePdfs(Product $product, int $nb = 1)
    {
        $this->storeAttachments($product, 'pdf', "{$product->id}-{$nb}-{locale}", 'pdf');
    }

    /**
     * Store one or multiple attachment(s) (image / video / pdf) in "storage/app/public/{$name}s/products/{$product->id}"
     *
     * @param Product       $product
     * @param string        $name
     * @param string        $file_name
     * @param string        $extension
     * @param callable|null $callback
     * @param int           $nb
     */
    private function storeAttachments(Product $product, string $name, string $file_name, string $extension, callable $callback = null, int $nb = 1)
    {
        if (request()->hasFile($name)) {
            if (is_array(request()->file($name))) {
                foreach (request()->file($name) as $locale => $attachment) {
                    $this->storeAttachment($attachment, $product, $name, str_replace_last('{locale}', $locale, $file_name), $extension, $callback, $nb);
                }
            } else {
                $this->storeAttachment(request()->file($name), $product, $name, $file_name, $extension, $callback, $nb);
            }
        }
    }

    /**
     * Store a given attachment in "storage/app/public/{$name}s/products/{$product->id}"
     *
     * @param               $attachment
     * @param Product       $product
     * @param string        $name
     * @param string        $file_name
     * @param string        $extension
     * @param callable|null $callback
     * @param int           $nb
     */
    private function storeAttachment($attachment, Product $product, string $name, string $file_name, string $extension, callable $callback = null, int $nb = 1)
    {
        if ($attachment->isValid()) {
            $path = $attachment->storeAs("{$name}s/products/{$product->id}", "{$file_name}.{$extension}", 'public');

            if (is_callable($callback)) {
                $callback($product, $path, $nb);
            }
        }
    }
}
