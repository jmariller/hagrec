<?php

namespace App\Http\Controllers;

use App\Reference;

class ReferencesController extends Controller
{

    /**
     * Main list of references
     *
     * @return Reference[]|\Illuminate\Contracts\View\Factory|\Illuminate\Database\Eloquent\Collection|\Illuminate\View\View
     */
    public function index()
    {
        $references = Reference::orderBy('sort_order', 'asc')->get();

        if (request()->has('json')) {
            return $references;
        }

        return view('references.index', compact('references'));
    }

    /**
     * Store a new reference
     */
    public function store()
    {
        request()->validate(['name' => 'required|unique:references,name']);

        return Reference::create([
            'name' => request('name'),
            'sort_order' => 999999
        ]);
    }

    /**
     * Update an existing reference
     *
     * @param Reference $reference
     * @return Reference
     */
    public function update(Reference $reference)
    {
        request()->validate(['name' => 'required']);

        $reference->update(['name' => request('name')]);

        return $reference;
    }

    /**
     * Update sort order for all products
     */
    public function updateAll()
    {
        $references = Reference::all();
        $nb_updates = 0;

        foreach (request('references') as $index => $reference) {
            $current_reference = $references->where('id', $reference['id'])->first();

            if ($current_reference->sort_order !== $index) {
                $current_reference->sort_order = $index;
                $current_reference->save();
                $nb_updates++;
            }
        }

        return $nb_updates;
    }

    /**
     * Delete a reference
     *
     * @param Reference $reference
     * @throws \Exception
     */
    public function destroy(Reference $reference)
    {
        $reference->delete();
        $reference->deleteLogo();
    }
}
