<?php

namespace App\Http\Controllers;

use App\Consumable;
use App\Http\Requests\StoreConsumable;
use Illuminate\Support\Facades\Storage;

class ConsumablesController extends Controller
{
    /**
     * ConsumablesController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    /**
     * Show list of consumables
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $consumables = Consumable::orderBy('sort_order', 'asc')->get();

        if (request()->has('json')) {
            return $consumables;
        }

        return view('consumables.index', compact('consumables'));
    }

    /**
     * Show the details for a given consumable
     *
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($slug)
    {
        $consumable = Consumable::whereTranslation('slug', $slug)->firstOrFail();

        if ($consumable->translate() != null && $consumable->translate()->where('slug', $slug)->first()->locale != app()->getLocale()) {
            return redirect()->route('consumable.show', $consumable->translate()->slug);
        }

        return view('consumables.show', compact('consumable'));
    }

    /**
     * Show the form to create a new consumable
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $consumable = new Consumable;

        return view('consumables.create', compact('consumable'));
    }

    /**
     * Save a new consumable
     *
     * @param StoreConsumable $form
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StoreConsumable $form)
    {
        $consumable = new Consumable;
        $consumable = $form->persist($consumable);
        $form->saveAttachments($consumable);

        session()->flash('message', 'Le consommable a bien été créé !');

        return redirect(route('consumables'));
    }

    /**
     * Show the form to edit an existing consumable
     *
     * @param Consumable $consumable
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Consumable $consumable)
    {
        return view('consumables.edit', compact('consumable'));
    }

    /**
     * Update an existing consumable and go back to that consumable
     *
     * @param StoreConsumable $form
     * @param Consumable $consumable
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(StoreConsumable $form, Consumable $consumable)
    {
        $form->persist($consumable);
        $form->saveAttachments($consumable);

        session()->flash('message', 'Le consommable a bien été mis à jour !');

        return redirect( route('consumable.edit', compact('consumable')) . request('anchor') );
    }

    /**
     * Update sort order for all consumables
     */
    public function updateAll()
    {
        $consumables = Consumable::all();
        $nb_updates = 0;

        foreach (request('consumables') as $index => $consumable) {
            $current_consumable = $consumables->where('id', $consumable['id'])->first();

            if ($current_consumable->sort_order !== $index) {
                $current_consumable->sort_order = $index;
                $current_consumable->save();
                $nb_updates++;
            }
        }

        return $nb_updates;
    }

    /**
     * Delete an existing consumable
     *
     * @param Consumable $consumable
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Consumable $consumable)
    {
        $consumable->delete();
        $this->deleteAttachments($consumable);

        session()->flash('message', 'Le consommable a bien été supprimé !');

        return redirect(route('consumables'));
    }

    /**
     * Delete any remaining attachments
     *
     * @param Consumable $consumable
     */
    private function deleteAttachments(Consumable $consumable)
    {
        Storage::deleteDirectory("public/images/consumables/{$consumable->id}");
    }

}
