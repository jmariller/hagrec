<?php

namespace App\Http\Controllers;

use App\Product;

class HomeController extends Controller
{
    public function index()
    {
        $highlight = Product::highlight();

        return view('home.index', compact('highlight'));
    }
}
