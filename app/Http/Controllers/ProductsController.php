<?php

namespace App\Http\Controllers;

use App\Product;
use App\Http\Requests\StoreProduct;
use Illuminate\Support\Facades\Storage;

class ProductsController extends Controller
{
    /**
     * ProductsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    /**
     * Main list of products
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $products = Product::orderBy('sort_order', 'asc')->get();

        if (request()->has('json')) {
            return $products;
        }

        return view('products.index', compact('products'));
    }

    /**
     * Show a product
     *
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show($slug)
    {
        $product = Product::whereTranslation('slug', $slug)->firstOrFail();

        if ($product->translate() != null && $product->translate()->where('slug', $slug)->first()->locale != app()->getLocale()) {
            return redirect()->route('product.show', $product->translate()->slug);
        }

        return view('products.show', compact('product'));
    }

    /**
     * Show the form to create a new product
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $product = new Product;

        return view('products.create', compact('product'));
    }

    /**
     * Save a new product
     *
     * @param StoreProduct $form
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StoreProduct $form)
    {
        $product = new Product;
        $product = $form->persist($product);
        $form->saveAttachments($product);

        session()->flash('message', 'Le produit a bien été créé !');

        return redirect(route('products'));
    }

    /**
     * Show the form to edit an existing product
     *
     * @param Product $product
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Product $product)
    {
        return view('products.edit', compact('product'));
    }

    /**
     * Update an existing product and go back to that product
     *
     * @param StoreProduct $form
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(StoreProduct $form, Product $product)
    {
        $form->persist($product);
        $form->saveAttachments($product);

        session()->flash('message', 'Le produit a bien été mis à jour !');

        return redirect( route('product.edit', compact('product')) . request('anchor') );
    }

    /**
     * Update sort order for all products
     */
    public function updateAll()
    {
        $products = Product::all();
        $nb_updates = 0;

        foreach (request('products') as $index => $product) {
            $current_product = $products->where('id', $product['id'])->first();

            if ($current_product->sort_order !== $index) {
                $current_product->sort_order = $index;
                $current_product->save();
                $nb_updates++;
            }
        }

        return $nb_updates;
    }

    /**
     * Delete an existing product
     *
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Product $product)
    {
        $product->delete();
        $this->deleteAttachments($product);

        session()->flash('message', 'Le produit a bien été supprimé !');

        return redirect(route('products'));
    }

    /**
     * Delete any remaining attachments
     *
     * @param Product $product
     */
    private function deleteAttachments(Product $product)
    {
        Storage::deleteDirectory("public/images/products/{$product->id}");
        Storage::deleteDirectory("public/pdfs/products/{$product->id}");
        Storage::deleteDirectory("public/videos/products/{$product->id}");
    }
}
