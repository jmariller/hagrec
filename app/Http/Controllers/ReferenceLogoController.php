<?php

namespace App\Http\Controllers;

use App\Reference;

class ReferenceLogoController extends Controller
{

    /**
     * Update the reference logo
     *
     * @param Reference $reference
     * @return Reference
     */
    public function store(Reference $reference)
    {
        $reference->deleteLogo();

        $reference->update(['logo_path' => $reference->fileName()]);

        request()->file('logo')->storeAs('images/references', $reference->fileName(), 'public');

        return $reference;
    }
    
}
