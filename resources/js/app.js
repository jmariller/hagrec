require('./bootstrap');

window.Vue = require('vue');

import ImageUpload from "./components/ImageUpload";
import ProductSortable from "./components/ProductSortable";
import ReferenceSortable from "./components/ReferenceSortable";
import ConsumableSortable from "./components/ConsumableSortable";

Vue.component('image-upload', ImageUpload);
Vue.component('product-sortable', ProductSortable);
Vue.component('reference-sortable', ReferenceSortable);
Vue.component('consumable-sortable', ConsumableSortable);