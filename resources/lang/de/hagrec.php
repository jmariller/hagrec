<?php

return [

    'slogan' => 'Recycling Anlagen',

    'switch_lang' => 'Sprache wechseln',
    'know_more' => 'Entdecken mehr...',

    'home' => 'Begrüssung',
    'products' => 'Produkten',
    'consumables' => 'Verbrauchsgüter',
    'references' => 'Referenzen',
    'contact' => 'Kontakt',

    'copyright' => 'Alle Rechte vorbehalten',
    'created_by' => 'Erstellt von',

    '404_title' => 'Seite nicht gefunden',
    '404_description' => 'Entschuldigung, die von Ihnen gesuchte Seite wurde nicht gefunden.',

    '500_title' => 'Interne Fehler',
    '500_description' => 'Beim Anzeigen dieser Seite ist ein Problem aufgetreten. Wenden Sie sich an den Administrator.',

    '503_title' => 'Website in Wartung',
    '503_description' => 'Die Seite wird gewartet, kommen Sie aber später wieder !',

];
