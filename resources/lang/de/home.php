<?php

return [

    'title' => 'Willkommen bei Hagrec SA!',
    'description' => '<p>Aktiv seit 2002, zunächst in Lausanne und dann bei Puidoux VD, hat Herr René Hagenbuch in 2018 die Firma Hagrec AG gegründet. Sein Ziel:</p><p>Maschinen und Dienstleistungen zur Reduzierung des Volumens von recycelbaren und verbrannten Materialien bereitstellen.</p><p>Heute bietet di Firam eine Reihe von Produkten, die von der heimischen Presse bis hin zu halbautomatischen Industriemaschinen reichen.</p><p>Die Einrichtungen sind im Showroom in Puidoux zu sehen.</p><p>Im Laufe der Zeit hat das Unternehmen Verbindungen zur Zusammenarbeit mit Nahverkehrsunternehmen, Mechanik, Recycling etc. geknüpft.</p><p>Im 2018 entwickelt und produziert Hagrec AG, in der Schweiz, seine eigene Maschine zur Reduzierung des Hausmüll Volumens. Ein völlig innovatives Konzept, das an die aktuellen Bedürfnisse angepasst ist. Mit diesen Pressen soll die Anzahl der Gebührsäcke von 10 auf 1 reduziert werden.</p>',

    'highlight' => 'Produkt des Tages',

    'see_our_products' => 'Unsere Produkten anschauen',
    'contact_us' => 'Uns kontaktieren',

];
