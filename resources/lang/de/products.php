<?php

return [

    'title' => 'Unsere Produkten',
    'introduction' => 'Klicken Sie auf einem Produkt, um mehr zu entdecken!',

    'tech_specs' => 'Technische Eigenschaften',
    'video' => 'Video',
    'sheet' => 'Produktdatenblatt',
    'open_pdf' => 'Dokument öffnen (PDF)',

    'back_to_list' => 'Zurück an der Liste der Produkten',

    'empty' => 'Momentan kein Produkt verfügbar.'

];
