<?php

return [

    'title' => 'Unsere Verbrauchsgüter',
    'introduction' => 'Klicken Sie auf einem Verbrauchsgut, um mehr zu entdecken!',
    'back_to_list' => 'Zurück an der Liste der Verbrauchsgüter',

    'empty' => 'Momentan kein Verbrauchsgut verfügbar.'

];
