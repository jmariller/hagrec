<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Wir können diesen Benutzer in unserer Datenbank nicht finden.',
    'throttle' => 'Zu viele Versuchen. Bitte probieren Sie nochmals in :seconds Sekunden.',

];
