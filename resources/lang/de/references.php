<?php

return [

    'title' => 'Referenzen',
    'introduction' => 'Auf dieser Seite sind die Unternehmen, die uns vertraut haben',
    'press_info' => 'Pressemeldungen',

    'empty' => 'Momentan kein Referenz verfügbar.'

];
