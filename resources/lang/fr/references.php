<?php

return [

    'title' => 'Références',
    'introduction' => 'Sur cette page se trouvent les entreprises qui nous ont fait confiance.',
    'press_info' => 'Informations de presse',

    'empty' => 'Aucune référence disponible pour le moment.'

];
