<?php

return [

    'title' => 'Nous contacter',

    'phone' => 'Tel.',
    'email' => 'E-mail',
    'web' => 'Web',

];
