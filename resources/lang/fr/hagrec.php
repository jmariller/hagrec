<?php

return [

    'slogan' => 'Equipements pour le recyclage',

    'switch_lang' => 'Changer langue',
    'know_more' => 'En savoir plus...',

    'home' => 'Accueil',
    'products' => 'Produits',
    'consumables' => 'Consommables',
    'references' => 'Références',
    'contact' => 'Contact',

    'copyright' => 'Tous droits réservés',
    'created_by' => 'Créé par',

    '404_title' => 'Page introuvable',
    '404_description' => 'Nous sommes désolés, la page que vous cherchez est introuvable.',

    '500_title' => 'Erreur interne',
    '500_description' => 'Nous sommes désolés, un problème est survenu lors de l\'affichage de cette page, veuillez contacter l\'administrateur.',

    '503_title' => 'Site en maintenance',
    '503_description' => 'Le site est en maintenance, mais revenez plus tard !',

];
