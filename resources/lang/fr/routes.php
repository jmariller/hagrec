<?php

return [

    'products' => 'produits',
    'consumables' => 'consommables',
    'references' => 'references',
    'contact' => 'contact',

];
