<?php

return [

    'title' => 'Nos produits',
    'introduction' => 'Cliquez sur un produit ci-dessous pour en savoir plus !',

    'tech_specs' => 'Caractéristiques techniques',
    'video' => 'Vidéo',
    'sheet' => 'Fiche produit',
    'open_pdf' => 'Ouvrir le document (PDF)',

    'back_to_list' => 'Retour à la liste des produits',

    'empty' => 'Aucun produit disponible pour le moment.'

];
