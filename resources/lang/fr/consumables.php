<?php

return [

    'title' => 'Nos consommables',
    'introduction' => 'Cliquez sur un consommable ci-dessous pour en savoir plus !',
    'back_to_list' => 'Retour à la liste des consommables',

    'empty' => 'Aucun consommable disponible pour le moment.'

];
