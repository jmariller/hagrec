<?php

return [

    'title' => 'Bienvenue chez Hagrec SA !',
    'description' => '<p>Etablie depuis 2002, tout d’abord à Lausanne puis à Puidoux VD, la société reste fidèle à son objectif:<br/>Fournir des machines et des prestations pour la diminutions du volume des matières recyclables et incinérables.</p><h3>Environnement protégé, Environnement conservé</h3><p>Aujourd\'hui elle offre une palette de produits allant de la presse domestique aux machines industrielles semi-automatiques.<br/>Des équipements sont visibles dans le Show-Room à Puidoux.</p><p>L\'entreprise, au fil des années, a tissé des liens de coopération avec les sociétés locales de transport, de mécanique, de recyclage...</p><p>En 2018 elle fabrique en Suisse sa propre machine pour la diminution du volume des déchets domestiques. Un concept innovant adapté aux besoins actuels. Ces presses sont prévues pour les sacs taxés de 17 lt, 60 lt et 110 lt.</p>',

    'highlight' => 'Le produit phare',

    'see_our_products' => 'Voir nos produits',
    'contact_us' => 'Nous contacter',

];
