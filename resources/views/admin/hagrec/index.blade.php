@extends ('admin.hagrec.master')

@section ('title', 'Administration Hagrec SA')

@section ('content')

    <!-- Content for administration -->
    <h3>Administration Hagrec SA</h3>

    <div class="alert alert-info" role="alert">
        <i class="fas fa-info-circle mr-2"></i> Sélectionnez un module dans le menu ci-dessus - pour le moment, seul le module <a class="alert-link" href="{{ route('admin.hagrec.suppliers') }}">Fournisseurs</a> est actif.
    </div>

    <div class="alert alert-warning" role="alert">
        <i class="fas fa-exclamation-triangle mr-2"></i> Cette section est en construction !
    </div>

    <hr />

@endsection