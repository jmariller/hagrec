@extends ('admin.hagrec.master')

@section ('title', 'Fournisseurs')

@section ('content')

    <!-- Content for administration -->
    <h3>Modifier un fournisseur  <span class="badge badge-pill badge-info">Démonstration</span></h3>

    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="id-tab" data-toggle="tab" href="#id" role="tab" aria-controls="id" aria-selected="true">
                <i class="fas fa-fingerprint mr-1"></i> Identification
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" id="address-tab" data-toggle="tab" href="#address" role="tab" aria-controls="address" aria-selected="false">
                <i class="fas fa-address-card mr-1"></i> Adresse
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">
                <i class="fas fa-phone mr-1"></i> Moyens de contact
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" id="notes-tab" data-toggle="tab" href="#notes" role="tab" aria-controls="notes" aria-selected="false">
                <i class="far fa-sticky-note mr-1"></i> Notes complémentaires
            </a>
        </li>

        <li class="ml-auto">
            <button type="submit" class="btn btn-link">
                <i class="far fa-save mr-1"></i> Enregistrer
            </button>

            <button type="reset" class="btn btn-link">
                <i class="fas fa-undo-alt"></i> Annuler
            </button>
        </li>
    </ul>

    <div class="tab-content py-3">

        <!-- ID tab -->
        <div class="tab-pane fade show active" id="id" role="tabpanel" aria-labelledby="id-tab">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Nom du fournisseur..." value="Ed. Keller AG" required />
            </div>

            <div class="form-group">
                <input type="text" class="form-control" placeholder="Alphabétique..." required />
            </div>

            <div class="form-group">
                <label for="supplier-language" class="sr-only">Langue:</label>
                <select class="form-control" id="supplier-language">
                    <option>Choisir une langue...</option>
                    <option>Français</option>
                    <option selected>Allemand</option>
                    <option>Anglais</option>
                    <option>Italien</option>
                </select>
            </div>

            <div class="form-group">
                <label for="supplier-domain" class="sr-only">Domaine:</label>
                <select class="form-control" id="supplier-domain">
                    <option>Choisir un domaine...</option>
                    <option>Alters-, Pflege- und Behindertenheim</option>
                    <option>Distribution</option>
                    <option>Fleurs et bouquets</option>
                    <option>...</option>
                </select>
            </div>

            <div class="form-group">
                <input type="text" class="form-control" placeholder="Anciennement..." />
            </div>
        </div>

        <!-- Address tab -->
        <div class="tab-pane fade" id="address" role="tabpanel" aria-labelledby="address-tab">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Rue..." value="Gerenstrasse 23" />
            </div>

            <div class="form-group">
                <input type="text" class="form-control" placeholder="Localité..." value="9533 Kirchberg" />
            </div>

            <div class="form-group">
                <label for="supplier-canton" class="sr-only">Canton:</label>
                <select class="form-control" id="supplier-canton">
                    <option>Choisir un canton...</option>
                    <option>Argovie</option>
                    <option selected>Saint-Gall</option>
                    <option>Vaud</option>
                    <option>Zurich</option>
                    <option>...</option>
                </select>
            </div>
        </div>

        <!-- Contact tab -->
        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Téléphone..." value="071 932 10 50" />
            </div>

            <div class="form-group">
                <input type="text" class="form-control" placeholder="Fax..." />
            </div>

            <div class="form-group">
                <input type="text" class="form-control" placeholder="Natel..." />
            </div>

            <div class="form-group">
                <input type="email" class="form-control" placeholder="Adresse email..." />
            </div>
        </div>

        <!-- Notes tab -->
        <div class="tab-pane fade" id="notes" role="tabpanel" aria-labelledby="notes-tab">
            <div class="form-group">
                <textarea class="form-control" placeholder="Commentaires concernant ce fournisseur..." rows="5"></textarea>
            </div>
        </div>
    </div>

    <div class="alert alert-warning mt-3" role="alert">
        Ceci n'est qu'une démonstration. Les données ne sont pas enregistrées dans la base de données.
    </div>

    <div class="mt-4">
        <a href="{{ route('admin.hagrec.suppliers') }}">
            <i class="far fa-arrow-alt-circle-left"></i> Retour à la liste des fournisseurs
        </a>
    </div>

@endsection