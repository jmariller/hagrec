@extends ('admin.hagrec.master')

@section ('title', 'Fournisseurs')

@section ('content')

    <!-- Content for administration -->
    <h3>Gestion des fournisseurs <span class="badge badge-pill badge-info">Démonstration</span></h3>

    <table class="table table-sm table-hover">
        <thead class="thead-light">
            <tr>
                <th scope="col">Nom</th>
                <th scope="col">Domaine</th>
                <th scope="col">Langue</th>
                <th scope="col">Localité</th>
                <th scope="col">Canton</th>
                <th scope="col">Téléphone</th>
                <th scope="col">Fax</th>
                <th scope="col" colspan="2" class="text-center">Gestion</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Ed. Keller AG</td>
                <td></td>
                <td>Allemand</td>
                <td>9533 Kirchberg</td>
                <td>SG</td>
                <td>071 932 10 50</td>
                <td></td>
                <td class="text-center">
                    <a href="{{ route('admin.hagrec.suppliers.edit') }}">
                        <i class="far fa-edit mr-2"></i>
                    </a>
                </td>
                <td class="text-center">
                    <a href="#" data-toggle="modal" data-target="#delete-supplier-modal">
                        <i class="far fa-trash-alt mr-2"></i>
                    </a>
                </td>
            </tr>
            <tr>
                <td>Un autre fournisseur</td>
                <td></td>
                <td>Français</td>
                <td>1000 Lausanne</td>
                <td>VD</td>
                <td>071 932 10 50</td>
                <td></td>
                <td class="text-center">
                    <a href="{{ route('admin.hagrec.suppliers.edit') }}">
                        <i class="far fa-edit mr-2"></i>
                    </a>
                </td>
                <td class="text-center">
                    <a href="#" data-toggle="modal" data-target="#delete-supplier-modal">
                        <i class="far fa-trash-alt mr-2"></i>
                    </a>
                </td>
            </tr>
            <tr>
                <td>Un altro fornitore</td>
                <td></td>
                <td>Italiano</td>
                <td>9000 Lugano</td>
                <td>TI</td>
                <td>091 879 65 43</td>
                <td></td>
                <td class="text-center">
                    <a href="{{ route('admin.hagrec.suppliers.edit') }}">
                        <i class="far fa-edit mr-2"></i>
                    </a>
                </td>
                <td class="text-center">
                    <a href="#" data-toggle="modal" data-target="#delete-supplier-modal">
                        <i class="far fa-trash-alt mr-2"></i>
                    </a>
                </td>
            </tr>
            <tr>
                <td>Another supplier</td>
                <td></td>
                <td>English</td>
                <td>8000 Zurich</td>
                <td>ZH</td>
                <td>058 857 45 37</td>
                <td></td>
                <td class="text-center">
                    <a href="{{ route('admin.hagrec.suppliers.edit') }}">
                        <i class="far fa-edit mr-2"></i>
                    </a>
                </td>
                <td class="text-center">
                    <a href="#" data-toggle="modal" data-target="#delete-supplier-modal">
                        <i class="far fa-trash-alt mr-2"></i>
                    </a>
                </td>
            </tr>
            <tr>
                <td colspan="9" class="text-center">...</td>
            </tr>
            <tr>
                <td colspan="9" class="text-center">...</td>
            </tr>
        </tbody>
    </table>

    <div class="modal fade" id="delete-supplier-modal" tabindex="-1" role="dialog" aria-labelledby="delete-supplier-modal-label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="delete-product-modal-label">Supprimer un fournisseur</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <p>Etes-vous certain de vouloir supprimer ce fournisseur ?</p>

                    <p class="alert alert-danger">Cette action est irréversible !</p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">
                        <i class="far fa-check-circle"></i> Oui
                    </button>

                    <button type="button" class="btn btn-link" data-dismiss="modal">
                        <i class="far fa-times-circle"></i> Non
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="alert alert-warning" role="alert">
        Ceci n'est qu'une démonstration. Chaque enregistrement dans la table ci-dessus pointe vers un même lien, et les données ne sont pas enregistrées dans la base de données.
    </div>

@endsection