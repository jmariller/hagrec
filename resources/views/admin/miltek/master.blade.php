<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>Mil-tek A/S @hasSection('title') - @yield('title') @endif</title>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width,user-scalable=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" />

    <script src="{{ asset('js/manifest.js') }}"></script>
    <script src="{{ asset('js/vendor.js') }}"></script>

    @auth
        <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
        <script src="{{ asset('js/app.js') }}"></script>
    @endauth

    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}" />
</head>
<body>

<div class="container-fluid p-3">
    @include('admin.miltek.header')
    @yield('content')
    @include('layouts.footer')
</div>

</body>
</html>