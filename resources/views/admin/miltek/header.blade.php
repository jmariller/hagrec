<!-- Header -->
<div class="container-fluid">

    <!-- Top toolbar -->
    @include('layouts.toptoolbar')

    <!-- Container for logo and site title -->
    <div class="row d-flex align-items-center">
        <a href="{{ route('admin.miltek') }}">
            <h1 class="miltek-main-logo text-hide mr-5">Mil-tek A/S</h1>
        </a>
        <h2>Panneau d'administration Mil-tek A/S</h2>
    </div>
</div>

<!-- Container for main menu -->
<nav class="nav miltek-main-menu text-md font-weight-bold">
    <a class="nav-link disabled" href="#">
        Fournisseurs
    </a>

    <a class="nav-link disabled" href="#">
        Vendeurs
    </a>

    <a class="nav-link disabled" href="#">
        Clients
    </a>

    <a class="nav-link disabled" href="#">
        Contrats
    </a>

    <a class="nav-link disabled" href="#">
        ...
    </a>
</nav>

<!-- Container for breadcrumb and quick search -->
<div class="row">
    {{ Breadcrumbs::render() }}
</div>