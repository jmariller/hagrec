@extends ('layouts.master')

@section ('title', 'Administration')

@section ('content')

    <!-- Content for administration -->
    <h3>Administration</h3>

    <div class="alert alert-info" role="alert">
        <i class="fas fa-info-circle mr-2"></i> Bienvenue dans le panneau d'administration, {{ auth()->user()->name }} ! Choisissez ci-dessous la société que vous souhaitez gérer.
    </div>

    <div class="alert alert-warning" role="alert">
        <i class="fas fa-exclamation-triangle mr-2"></i> Pour la gestion des produits <q>Hagrec SA</q> veuillez suivre le lien <a href="{{ route('products') }}" class="alert-link">Produits</a> ci-dessus.
    </div>

    <div class="row">
        <div class="card text-center m-3">
            <div class="card-body">
                <a href="{{ route('admin.hagrec') }}">
                    <img class="img-fluid" src="{{ asset('images/general/hagrec_logo.png') }}" alt="Hagrec SA" />
                </a>
            </div>

            <div class="card-footer">
                <a class="card-title card-link" href="{{ route('admin.hagrec') }}">Hagrec SA</a>
            </div>
        </div>

        <div class="card text-center m-3">
            <div class="card-body">
                <a href="{{ route('admin.miltek') }}">
                    <img class="img-fluid" src="{{ asset('images/general/miltek_logo.png') }}" alt="Mil-tek A/S" />
                </a>
            </div>

            <div class="card-footer">
                <a class="card-title card-link" href="{{ route('admin.miltek') }}">Mil-tek A/S</a>
            </div>
        </div>
    </div>

    <hr />

@endsection