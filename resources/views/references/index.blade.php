@extends ('layouts.master')

@section ('title', __('hagrec.references'))

@section ('content')

    <!-- Content for references -->
    <h3>{{ __('references.title') }}</h3>

    <p>{{ __('references.introduction') }}</p>

    <p>
        <a href="{{ asset('storage/pdfs/references/press_information_' . app()->getLocale() . '.pdf') }}">
            <i class="fas fa-info-circle mr-1"></i> {{ __('references.press_info') }}
        </a>
    </p>

    <p>
        <a href="{{ asset('storage/pdfs/references/material_handling.pdf') }}">
            <i class="far fa-newspaper mr-1"></i> Material Handling
        </a>
    </p>

    @auth
        @if ($message = session('message'))
            <div class="alert alert-success">
                {{ $message  }}
            </div>
        @endif

        <div id="references-list-administration">

            <div class="alert alert-info" role="alert">
                <i class="fas fa-info-circle mr-1"></i> Maintenez le bouton gauche de la souris sur une référence pour changer sa position dans la liste.
            </div>

            <reference-sortable></reference-sortable>
        </div>

        <script>
            new Vue({
                el: '#references-list-administration',
            })
        </script>
    @endauth

    @guest
        @if (count($references) == 0)
            <div class="alert alert-info">
                {{ __('references.empty') }}
            </div>
        @else
            <div class="row">
                @foreach ($references as $reference)
                    @if ($reference->logo_path)
                        <div class="card reference text-center m-3">
                            <div class="card-body">
                                <img class="card-img-top img-fluid" src="{{ $reference->pathToLogo() }}" alt="{{ $reference->name }}">
                            </div>

                            <div class="card-footer">
                                <p class="my-0">
                                    {{ $reference->name }}
                                </p>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        @endif
    @endguest

@endsection