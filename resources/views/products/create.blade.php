@extends ('layouts.master')

@section ('title', 'Produit - Créer nouveau')

@section ('content')

    <!-- Details for a new product -->
    <h3>Créer un nouveau produit</h3>

    @include ('products.header')

    <form method="POST" action="{{ route('product.store') }}" enctype="multipart/form-data">
        <div>
            @csrf

            @include ('products.form')
        </div>
    </form>

    @include ('products._script')

@endsection