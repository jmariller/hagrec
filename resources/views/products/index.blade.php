@extends ('layouts.master')

@section ('title', __('hagrec.products'))

@section ('content')

    <!-- Content for products -->
    <h3>{{ __('products.title') }}</h3>

    <p>{{ __('products.introduction') }}</p>

    @auth
        @if ($message = session('message'))
            <div class="alert alert-success">
                {{ $message  }}
            </div>
        @endif

        <div id="products-list-administration">
            <hr/>

            <p>
                <a href="{{ route('product.create') }}" class="button">
                    <i class="fas fa-plus mr-1"></i> Créer un nouveau produit
                </a>
            </p>

            <hr/>

            <div class="alert alert-info" role="alert">
                <i class="fas fa-info-circle mr-1"></i> Maintenez le bouton gauche de la souris sur un produit pour changer sa position dans la liste.
            </div>

            <product-sortable></product-sortable>
        </div>

        <script>
            new Vue({
                el: '#products-list-administration',
            })
        </script>
    @endauth

    @guest
        @if (count($products) == 0)
            <div class="alert alert-info">
                {{ __('products.empty') }}
            </div>
        @else
            <!-- List of products -->
            <div class="row products-list">
                @foreach ($products as $product)
                    <div class="card text-center m-3">
                        <div class="card-body">
                            <a href="{{ route('product.show', $product->slug) }}">
                                <img class="img-fluid" src="{{ $product->image(1, 'small') }}" alt="{{ $product->name }}" />
                            </a>
                        </div>

                        <div class="card-footer">
                            <a class="card-title card-link" href="{{ route('product.show', $product->slug) }}">{{ $product->name }}</a>
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
    @endguest

@endsection