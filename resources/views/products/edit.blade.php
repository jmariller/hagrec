@extends ('layouts.master')

@section ('title', 'Modifier "' . $product->name . '"')

@section ('content')

    <!-- Details for an existing product -->
    <h3>Modifier le produit <q>{{ $product->name }}</q></h3>

    @include ('products.header')

    <form method="POST" action="{{ route('product.update', $product) }}" enctype="multipart/form-data" id="product_form">
        <div>
            @csrf
            @method('PUT')

            @include ('products.form')
        </div>
    </form>

    @include ('products._script')

@endsection