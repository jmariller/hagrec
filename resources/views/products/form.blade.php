<input type="hidden" name="anchor" id="anchor" value="" />

@if ($product->id != null && $product->highlight)
    <p class="alert alert-info">
        <i class="fas fa-info-circle mr-1"></i> Ceci est le produit phare. Sélectionnez un autre produit et cochez la case <q>Produit phare</q>.
    </p>
@else
    <div class="form-group custom-control custom-checkbox my-3">
        <input type="checkbox" class="custom-control-input" name="highlight" id="highlight">
        <label class="custom-control-label" for="highlight">Produit phare</label>
    </div>
@endif

<!-- Tabs bar -->
<ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="fr-tab" data-toggle="tab" href="#fr" role="tab" aria-controls="fr" aria-selected="true">
            <i class="fas fa-globe mr-1"></i> Français
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" id="de-tab" data-toggle="tab" href="#de" role="tab" aria-controls="de" aria-selected="false">
            <i class="fas fa-globe mr-1"></i> Allemand
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" id="image-tab" data-toggle="tab" href="#image" role="tab" aria-controls="image" aria-selected="false">
            <i class="far fa-image mr-1"></i> Image
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" id="video-tab" data-toggle="tab" href="#video" role="tab" aria-controls="video" aria-selected="false">
            <i class="far fa-file-video mr-1"></i> Vidéo
        </a>
    </li>

    <li class="ml-auto">
        <button type="submit" class="btn btn-link">
            <i class="far fa-save mr-1"></i> Enregistrer
        </button>

        <button type="reset" class="btn btn-link">
            <i class="fas fa-undo-alt"></i> Annuler
        </button>
    </li>
</ul>



<!-- Content per tab -->
<div class="tab-content py-3">

    <!-- French tab -->
    <div class="tab-pane fade show active" id="fr" role="tabpanel" aria-labelledby="fr-tab">

        <div class="form-group">
            <label class="sr-only" for="name_fr">Nom:</label>
            <input type="text" class="form-control" name="name[fr]" id="name_fr" value="{{ $product->translateOrNew('fr')->name }}" placeholder="Nom du produit..." />
        </div>

        <div class="form-group">
            <label for="description_fr">Description:</label>
            <textarea name="description[fr]" id="description_fr" class="form-control text-editor">{{ $product->translateOrNew('fr')->description }}</textarea>
        </div>

        <div class="form-group">
            <label for="tech_specs_fr">Caractéristiques techniques:</label>
            <textarea name="tech_specs[fr]" id="tech_specs_fr" class="form-control text-editor">{{ $product->translateOrNew('fr')->tech_specs }}</textarea>
        </div>

        <div class="form-group">
            <label for="other_info_fr">Autres informations:</label>
            <textarea name="other_info[fr]" id="other_info_fr" class="form-control text-editor">{{ $product->translateOrNew('fr')->other_info }}</textarea>
        </div>

        <div class="custom-file">
            <input type="file" class="custom-file-input" id="pdf_fr" name="pdf[fr]" accept="application/pdf" />
            <label class="custom-file-label" for="pdf_fr">Choisir un fichier PDF</label>
        </div>
    </div>

    <!-- German tab -->
    <div class="tab-pane fade" id="de" role="tabpanel" aria-labelledby="de-tab">
        <div class="form-group">
            <label class="sr-only" for="name_de">Nom:</label>
            <input type="text" class="form-control" name="name[de]" id="name_de" value="{{ $product->translateOrNew('de')->name }}" placeholder="Nom du produit..." />
        </div>

        <div class="form-group">
            <label for="description_de">Description:</label>
            <textarea name="description[de]" id="description_de" class="form-control text-editor">{{ $product->translateOrNew('de')->description }}</textarea>
        </div>

        <div class="form-group">
            <label for="tech_specs_de">Caractéristiques techniques:</label>
            <textarea name="tech_specs[de]" id="tech_specs_de" class="form-control text-editor">{{ $product->translateOrNew('de')->tech_specs }}</textarea>
        </div>

        <div class="form-group">
            <label for="other_info_de">Autres informations:</label>
            <textarea name="other_info[de]" id="other_info_de" class="form-control text-editor">{{ $product->translateOrNew('de')->other_info }}</textarea>
        </div>

        <div class="custom-file">
            <input type="file" class="custom-file-input" id="pdf_de" name="pdf[de]" accept="application/pdf" />
            <label class="custom-file-label" for="pdf_de">Choisir un fichier PDF</label>
        </div>
    </div>
    
    <!-- Image tab -->
    <div class="tab-pane fade" id="image" role="tabpanel" aria-labelledby="image-tab">
        <div class="card">
            <div class="card-body">
                <img src="{{ $product->image(1, 'small') }}" alt="" />
            </div>
            <div class="card-footer">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="image" name="image" accept="image/*" />
                    <label class="custom-file-label" for="image">Choisir une image</label>
                </div>
            </div>
        </div>
    </div>

    <!-- Video tab -->
    <div class="tab-pane fade" id="video" role="tabpanel" aria-labelledby="video-tab">
        <div class="card">
            <div class="card-body">
                @if (($video_path = $product->video()) != false )
                    <video width="320" height="240" controls preload="auto">
                        <source src="{{ $video_path }}" type="video/mp4">
                    </video>
                @else
                    <p class="alert alert-warning">
                        Aucune vidéo n'a été chargée pour ce produit.
                    </p>
                @endif
            </div>
            <div class="card-footer">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="video" name="video" accept="video/mp4" />
                    <label class="custom-file-label" for="image">Choisir une vidéo</label>
                </div>
            </div>
        </div>
    </div>
</div>