@extends ('layouts.master')

@section ('title', __('hagrec.products') . ' - ' . $product->name)

@section ('content')

    @auth
        <hr />

        <a class="btn btn-link mr-4" href="{{ route('product.edit', $product) }}">
            <i class="far fa-edit mr-2"></i> Modifier ce produit
        </a>

        <button type="button" class="btn btn-link" data-toggle="modal" data-target="#delete-product-modal">
            <i class="far fa-trash-alt mr-2"></i> Supprimer ce produit
        </button>

        <hr />

        <div class="modal fade" id="delete-product-modal" tabindex="-1" role="dialog" aria-labelledby="delete-product-modal-label" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="delete-product-modal-label">Supprimer <q>{{ $product->name }}</q></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <p>Etes-vous certain de vouloir supprimer ce produit ?</p>

                        <p class="alert alert-danger">Cette action est irréversible !</p>
                    </div>

                    <div class="modal-footer">
                        <form method="POST" action="{{ route('product.destroy', $product) }}" id="form-to-delete-product">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-link" id="btn-confirm-delete">
                                <i class="far fa-check-circle"></i> Oui
                            </button>
                        </form>

                        <button type="button" class="btn btn-link" data-dismiss="modal">
                            <i class="far fa-times-circle"></i> Non
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $('#form-to-delete-product').submit(function(event) {
                $('#delete-product-modal').modal('hide');
                return true;
            });
        </script>
    @endauth

    <!-- Details for a given product -->
    <div class="row">

        <div class="col-sm-6">
            <h3>{{ $product->name }}</h3>

            <a href="{{ $product->image(1) }}">
                <img class="img-fluid" src="{{ $product->image(1, 'small') }}" alt="{{ $product->name }}" />
            </a>

            <p>{!! $product->description !!}</p>
        </div>

        <div class="col">
            <h3>{{ __('products.tech_specs') }}</h3>

            <div>{!! $product->tech_specs !!}</div>

            <div>{!! $product->other_info !!}</div>
        </div>

        <div class="col">
            @if (($video_path = $product->video()) != false )
                <h3>{{ __('products.video') }}</h3>

                <video width="320" height="240" controls preload="auto">
                    <source src="{{ $video_path }}" type="video/mp4">
                    {{ __('products.video_unsupported') }}
                </video>

                <hr />
            @endif

            @if (($pdf_path = $product->pdf(1, app()->getLocale())) != false )
                <h3>{{ __('products.sheet') }}</h3>

                <p>
                    <a href="{{ $pdf_path }}">
                        {{ __('products.open_pdf') }}
                    </a>
                </p>
            @endif
        </div>

    </div>

    <!-- Go back to list of products -->
    <div class="mt-4">
        <a href="{{ route('products') }}">
            <i class="far fa-arrow-alt-circle-left"></i> {{ __('products.back_to_list') }}
        </a>
    </div>

@endsection