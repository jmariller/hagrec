<nav class="nav top-toolbar text-sm justify-content-end">

@auth
    <!-- The signed in administrator -->
        <form method="POST" action="{{ route('logout') }}" class="signed-in-administrator">
            @csrf

            <a class="btn btn-link text-sm" href="{{ route('admin') }}">
                <i class="fas fa-toolbox mr-1"></i> Panneau d'administration
            </a>

            <button type="submit" class="btn btn-link text-sm">
                <i class="fas fa-sign-out-alt mr-1"></i> Se déconnecter ({{ auth()->user()->name }})
            </button>
        </form>
    @endauth

    <div class="dropdown">
        <button class="btn btn-link dropdown-toggle text-sm" type="button" id="drop-down-languages" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-globe mr-1"></i> {{ config('translatable.locales')[app()->getLocale()] }}
        </button>

        <div class="dropdown-menu" aria-labelledby="drop-down-languages">
            @foreach (config('translatable.locales') as $lang => $language)
                <a class="dropdown-item btn btn-link text-sm {{ $lang == app()->getLocale() ? 'disabled' : '' }}" href="{{ route('lang.switch', $lang) }}">
                    {{ $language }}
                </a>
            @endforeach
        </div>
    </div>
</nav>