<!-- Header -->
<div class="container-fluid">

    <!-- Top toolbar -->
    @include('layouts.toptoolbar')

    <!-- Container for logo and site title -->
    <div class="row d-flex align-items-center">
        <a href="/">
            <h1 class="hagrec-main-logo text-hide mr-5">{{ config('app.name') }}</h1>
        </a>
        <h2>{{ __('hagrec.slogan') }}</h2>
    </div>
</div>

<!-- Container for main menu -->
<nav class="nav hagrec-main-menu text-md font-weight-bold">
    <a class="nav-link" href="{{ route('home') }}">
        {{ __('hagrec.home') }}
    </a>

    <a class="nav-link" href="{{ route('products') }}">
        {{ __('hagrec.products') }}
    </a>

    {{--<a class="nav-link" href="{{ route('references') }}">--}}
        {{--{{ __('hagrec.references') }}--}}
    {{--</a>--}}

    {{--<a class="nav-link" href="{{ route('consumables') }}">--}}
        {{--{{ __('hagrec.consumables') }}--}}
    {{--</a>--}}

    <a class="nav-link" href="{{ route('contact') }}">
        {{ __('hagrec.contact') }}
    </a>
</nav>

<!-- Container for breadcrumb and quick search -->
<div class="row">
    {{ Breadcrumbs::render() }}
</div>