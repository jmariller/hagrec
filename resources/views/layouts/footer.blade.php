<footer class="text-sm text-center justify-content-center mt-5">
    © {{ (date('Y') == 2018 ? 2018 : '2018 - '.date('Y')) . ' ' . config('app.name') . '. ' . __('hagrec.copyright') . '.' }}
    <!-- Créé par <a href="https://mariller.ch/julien">Julien Mariller</a> -->
</footer>