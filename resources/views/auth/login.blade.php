@extends ('layouts.master')

@section ('content')

    <!-- Content for administration -->
    <h3>Login</h3>

    <form method="POST" action="{{ route('login') }}">
        @csrf

        <div class="form-group">
            <label for="email" class="sr-only">Email:</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Email..." required />
        </div>

        <div class="form-group">
            <label for="password" class="sr-only">Mot de passe:</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Mot de passe..." required />
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-link">
                <i class="fas fa-sign-in-alt mr-2"></i> Se connecter
            </button>
        </div>

        <hr/>

        @include ('layouts.errors')
    </form>

@endsection