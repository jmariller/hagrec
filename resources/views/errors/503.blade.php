@extends ('layouts.master')

@section ('content')

    <!-- Content for maintenance mode (503) -->
    <h3>
        {{ __('hagrec.503_title') }}
    </h3>

    <p>
        {{ __('hagrec.503_description') }}
    </p>

@endsection