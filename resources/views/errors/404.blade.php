@extends ('layouts.master')

@section ('content')

    <!-- Content for error 404 -->
    <h3>
        {{ __('hagrec.404_title') }}
    </h3>

    <p>
        {{ __('hagrec.404_description') }}
    </p>

@endsection