@extends ('layouts.master')

@section ('content')

    <!-- Content for error 500 -->
    <h3>
        {{ __('hagrec.500_title') }}
    </h3>

    <p>
        {{ __('hagrec.500_description') }}
    </p>

@endsection