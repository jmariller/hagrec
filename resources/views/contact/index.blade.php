@extends ('layouts.master')

@section ('title', __('hagrec.contact'))

@section ('content')

    <!-- Content for contacts -->
    <h3>{{ __('contact.title') }}</h3>

    <div class="row">

        <!-- Physical contact details -->
        <div class="col-sm contact-details">
            <p class="p-0 m-0">Hagrec SA</p>
            <p class="p-0 m-0">Rte du Vergnolet 8E</p>
            <p class="p-0 m-0">1070 Puidoux</p>
        </div>

        <!-- Media contact details -->
        <div class="col-sm contact-details">
            <p class="p-0 m-0">
                <span class="hagrec-label">{{ __('contact.phone') }}</span>021 729 80 88
            </p>

            <p class="p-0 m-0">
                <span class="hagrec-label">{{ __('contact.email') }}</span><?php echo 'info@hagrec.ch'; ?>
            </p>

            <p class="p-0 m-0">
                <span class="hagrec-label">{{ __('contact.web') }}</span><a href="https://www.hagrec.ch">www.hagrec.ch</a>
            </p>
        </div>

    </div>

    <hr />

    <!-- Map to find us -->
    <img class="img-fluid" src="{{ asset('images/contact/map.jpg') }}" alt="" width="458" height="398" />

@endsection