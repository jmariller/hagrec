<input type="hidden" name="anchor" id="anchor" value="" />

<!-- Tabs bar -->
<ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="fr-tab" data-toggle="tab" href="#fr" role="tab" aria-controls="fr" aria-selected="true">
            <i class="fas fa-globe mr-1"></i> Français
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" id="de-tab" data-toggle="tab" href="#de" role="tab" aria-controls="de" aria-selected="false">
            <i class="fas fa-globe mr-1"></i> Allemand
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" id="image-tab" data-toggle="tab" href="#image" role="tab" aria-controls="image" aria-selected="false">
            <i class="far fa-image mr-1"></i> Image
        </a>
    </li>

    <li class="ml-auto">
        <button type="submit" class="btn btn-link">
            <i class="far fa-save mr-1"></i> Enregistrer
        </button>

        <button type="reset" class="btn btn-link">
            <i class="fas fa-undo-alt"></i> Annuler
        </button>
    </li>
</ul>

<!-- Content per tab -->
<div class="tab-content py-3">

    <!-- French tab -->
    <div class="tab-pane fade show active" id="fr" role="tabpanel" aria-labelledby="fr-tab">

        <div class="form-group">
            <label class="sr-only" for="name_fr">Nom:</label>
            <input type="text" class="form-control" name="name[fr]" id="name_fr" value="{{ $consumable->translateOrNew('fr')->name }}" placeholder="Nom du consommable..." />
        </div>

        <div class="form-group">
            <label for="description_fr">Description:</label>
            <textarea name="description[fr]" id="description_fr" class="form-control text-editor">{{ $consumable->translateOrNew('fr')->description }}</textarea>
        </div>
    </div>

    <!-- German tab -->
    <div class="tab-pane fade" id="de" role="tabpanel" aria-labelledby="de-tab">
        <div class="form-group">
            <label class="sr-only" for="name_de">Nom:</label>
            <input type="text" class="form-control" name="name[de]" id="name_de" value="{{ $consumable->translateOrNew('de')->name }}" placeholder="Nom du consommable..." />
        </div>

        <div class="form-group">
            <label for="description_de">Description:</label>
            <textarea name="description[de]" id="description_de" class="form-control text-editor">{{ $consumable->translateOrNew('de')->description }}</textarea>
        </div>
    </div>
    
    <!-- Image tab -->
    <div class="tab-pane fade" id="image" role="tabpanel" aria-labelledby="image-tab">
        <div class="card">
            <div class="card-body">
                <img src="{{ $consumable->image(1, 'small') }}" alt="" />
            </div>
            <div class="card-footer">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="image" name="image" accept="image/*" />
                    <label class="custom-file-label" for="image">Choisir une image</label>
                </div>
            </div>
        </div>
    </div>
</div>