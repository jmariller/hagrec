@extends ('layouts.master')

@section ('title', 'Modifier "' . $consumable->name . '"')

@section ('content')

    <!-- Details for an existing consumable -->
    <h3>Modifier le consommable <q>{{ $consumable->name }}</q></h3>

    @include ('consumables.header')

    <form method="POST" action="{{ route('consumable.update', $consumable) }}" enctype="multipart/form-data" id="consumable_form">
        <div>
            @csrf
            @method('PUT')

            @include ('consumables.form')
        </div>
    </form>

    @include ('consumables._script')

@endsection