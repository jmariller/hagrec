@extends ('layouts.master')

@section ('title', 'Consommable - Créer nouveau')

@section ('content')

    <!-- Details for a new consumable -->
    <h3>Créer un nouveau consommable</h3>

    @include ('consumables.header')

    <form method="POST" action="{{ route('consumable.store') }}" enctype="multipart/form-data">
        <div>
            @csrf

            @include ('consumables.form')
        </div>
    </form>

    @include ('consumables._script')

@endsection