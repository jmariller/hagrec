@extends ('layouts.master')

@section ('title', __('hagrec.consumables') . ' - ' . $consumable->name)

@section ('content')

    @auth
        <hr />

        <a class="btn btn-link mr-4" href="{{ route('consumable.edit', $consumable) }}">
            <i class="far fa-edit mr-2"></i> Modifier ce consommable
        </a>

        <button type="button" class="btn btn-link" data-toggle="modal" data-target="#delete-consumable-modal">
            <i class="far fa-trash-alt mr-2"></i> Supprimer ce consommable
        </button>

        <hr />

        <div class="modal fade" id="delete-consumable-modal" tabindex="-1" role="dialog" aria-labelledby="delete-consumable-modal-label" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="delete-consumable-modal-label">Supprimer <q>{{ $consumable->name }}</q></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <p>Etes-vous certain de vouloir supprimer ce consommable ?</p>

                        <p class="alert alert-danger">Cette action est irréversible !</p>
                    </div>

                    <div class="modal-footer">
                        <form method="POST" action="{{ route('consumable.destroy', $consumable) }}" id="form-to-delete-consumable">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-link" id="btn-confirm-delete">
                                <i class="far fa-check-circle"></i> Oui
                            </button>
                        </form>

                        <button type="button" class="btn btn-link" data-dismiss="modal">
                            <i class="far fa-times-circle"></i> Non
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $('#form-to-delete-consumable').submit(function(event) {
                $('#delete-consumable-modal').modal('hide');
                return true;
            });
        </script>
    @endauth

    <!-- Details for a given consumable -->
    <div class="row">
        <div class="col-sm-4">
            <a href="{{ $consumable->image(1) }}">
                <img class="img-fluid" src="{{ $consumable->image(1, 'small') }}" alt="{{ $consumable->name }}" />
            </a>
        </div>

        <div class="col">
            <h3>{{ $consumable->name }}</h3>

            <p>{!! $consumable->description !!}</p>
        </div>

    </div>

    <!-- Go back to list of consumables -->
    <div class="mt-4">
        <a href="{{ route('consumables') }}">
            <i class="far fa-arrow-alt-circle-left"></i> {{ __('consumables.back_to_list') }}
        </a>
    </div>

@endsection