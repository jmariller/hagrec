@extends ('layouts.master')

@section ('title', __('hagrec.consumables'))

@section ('content')

    <!-- Content for consumables -->
    <h3>{{ __('consumables.title') }}</h3>

    <p>{{ __('consumables.introduction') }}</p>

    @auth
        @if ($message = session('message'))
            <div class="alert alert-success">
                {{ $message  }}
            </div>
        @endif

        <div id="consumables-list-administration">
            <hr/>

            <p>
                <a href="{{ route('consumable.create') }}" class="button">
                    <i class="fas fa-plus mr-1"></i> Créer un nouveau consommable
                </a>
            </p>

            <hr/>

            <div class="alert alert-info" role="alert">
                <i class="fas fa-info-circle mr-1"></i> Maintenez le bouton gauche de la souris sur un consommable pour changer sa position dans la liste.
            </div>

            <consumable-sortable></consumable-sortable>
        </div>

        <script>
            new Vue({
                el: '#consumables-list-administration',
            })
        </script>
    @endauth

    @guest
        @if (count($consumables) == 0)
            <div class="alert alert-info">
                {{ __('consumables.empty') }}
            </div>
        @else
            <!-- List of consumables -->
            <div class="row consumables-list">
                @foreach ($consumables as $consumable)
                    <div class="card text-center m-3">
                        <div class="card-body">
                            <a href="{{ route('consumable.show', $consumable->slug) }}">
                                <img class="img-fluid" src="{{ $consumable->image(1, 'small') }}" alt="{{ $consumable->name }}" />
                            </a>
                        </div>

                        <div class="card-footer">
                            <a class="card-title card-link" href="{{ route('consumable.show', $consumable->slug) }}">{{ $consumable->name }}</a>
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
    @endguest

@endsection