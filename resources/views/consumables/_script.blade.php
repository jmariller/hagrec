<script>
    CKEDITOR.replaceAll('text-editor');

    // Set selected tab based on location hash
    if (location.hash !== '') {
        $(location.hash + '-tab').tab('show');
        $('#anchor').val(location.hash);
    }

    // Listen for tab clicking
    $('.nav-tabs .nav-link').click(function() {
        location.hash = $(this).attr('href');
        $('#anchor').val(location.hash);
    });
</script>