@extends ('layouts.master')

@section ('content')

    <!-- Content for home page -->
    <div class="row justify-content-between">

        <!-- Welcome area with quick links -->
        <div class="col-lg-8">
            <h3>{{ __('home.title') }}</h3>

            <p>{!! __('home.description') !!}</p>

            <!-- Quick links -->
            <div class="row justify-content-center my-3">
                <div class="col-sm-2">
                    <a class="d-block text-center" href="{{ route('products') }}">
                        <img class="img-fluid" src="{{ asset('images/general/products_icon.png') }}" alt="{{ __('home.see_our_products') }}" width="137" height="120" />
                        <p class="mt-3">{{ __('home.see_our_products') }}</p>
                    </a>
                </div>
                <div class="col-sm-2">
                    <a class="d-block text-center" href="{{ route('contact') }}">
                        <img class="img-fluid" src="{{ asset('images/general/contacts_icon.png') }}" alt="{{ __('home.contact_us') }}" width="121" height="120" />
                        <p class="mt-3">{{ __('home.contact_us') }}</p>
                    </a>
                </div>
            </div>
        </div>

        @if ($highlight)
            <div class="col-lg-4 text-center">
                <h3>{{ __('home.highlight') }}: {{ $highlight->name }}</h3>
                <figure>
                    <a href="{{ route('product.show', $highlight->slug) }}">
                        <img class="img-fluid" src="{{ $highlight->image(1, 'small') }}" alt="{{ $highlight->name }}" height="300" />
                    </a>
                </figure>
                <p>
                    <a href="{{ route('product.show', $highlight->slug) }}">{{ __('hagrec.know_more') }}</a>
                </p>
            </div>
        @endif

    </div>

@endsection